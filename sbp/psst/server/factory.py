from sbp.psst.common.factory import create
from sbp.psst.server.machine import Machine

def create_server(*args, **kwargs):
    return create(Machine, *args, **kwargs)
