from playground.network.common.MessageHandler import \
        SimpleMessageHandlingProtocol
from playground.network.common.Protocol import StackingProtocolMixin
from playground.network.client.ClientApplicationServer import \
        ClientApplicationServer
from sbp.psst.common.messages.base import from_playground_msg, message_types
from sbp.psst.common.config import Config
from sbp.psst.server.factory import create_server as create_base
from sbp.ptcl.server.protocol import create_server as make_reliable


class PsstServerProtocol(SimpleMessageHandlingProtocol,
                         StackingProtocolMixin):
    """The server protocol implementation for PSST."""
    def __init__(self, *args, **kwargs):
        SimpleMessageHandlingProtocol.__init__(self, *args, **kwargs)

        for message_type in message_types:
            self.registerMessageHandler(message_type,
                                        self._handle_psst_message)

        self._config = Config.load()

    def connectionMade(self):
        self._server = create_base(self.transport, self.getHigherProtocol(),
                                   self._config.cert_chain,
                                   self._config.private_key)
        self._server.start()

    def _handle_psst_message(self, protocol, msg):
        psst_msg = from_playground_msg(msg)

        if psst_msg:
            self._server._on_message(psst_msg)


class PsstServer(ClientApplicationServer):
    """The server implemention for PSST."""
    Protocol = PsstServerProtocol


def create_server(higher_factory):
    """Creates and returns a new PSST server."""
    factory = PsstServer()
    factory.setHigherFactory(higher_factory)
    return make_reliable(factory)
