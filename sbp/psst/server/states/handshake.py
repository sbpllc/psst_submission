from sbp.psst.common.state import NestedStateMachine, BaseState
from sbp.psst.common.states.data import DataState
from sbp.psst.common.handshake_crypto import HandshakeCrypto, PublicKeyCrypto
from sbp.psst.common.crypto import InvalidKeyException, InvalidNonceException
from sbp.psst.common.encrypted_transport import EncryptedTransport
from sbp.psst.common.messages.handshake import HelloMessage, \
        ChallengeMessage, ChallengeResponseMessage, ResponseMessage, \
        ErrorMessage
from sbp.psst.common.util import secure_string_equals


class HandshakeState(NestedStateMachine):
    def __init__(self, transport, key_store, private_key, *args, **kwargs):
        public_key = PublicKeyCrypto()
        self.crypto = HandshakeCrypto(private_key, public_key)
        encrypted_transport = EncryptedTransport(transport, self.crypto)

        super(HandshakeState, self).__init__([
            HelloWaitState(encrypted_transport, public_key),
            ChallengeState(encrypted_transport, private_key, key_store),
            ChallengeResponseWaitState(encrypted_transport, key_store),
            ResponseState(encrypted_transport, key_store),
        ], *args, **kwargs)

    def _on_message(self, msg):
        decrypted_msg = self.crypto.decrypt(msg)
        super(HandshakeState, self)._on_message(decrypted_msg)

    def accept(self):
        super(HandshakeState, self).accept()
        self.machine.transition(DataState)

    def reject(self):
        super(HandshakeState, self).reject()
        self.machine.reject()


class HelloWaitState(BaseState):
    def __init__(self, transport, client_crypto, *args, **kwargs):
        super(HelloWaitState, self).__init__(*args, **kwargs)
        self.transport, self.client_crypto = transport, client_crypto

    def _on_message(self, msg):
        if isinstance(msg, HelloMessage):
            # TODO validiate cert chain
            if True:
                self.client_crypto.set_public_key(msg.cert_chain)
                self.machine.transition(ChallengeState)
            else:
                error_msg = ErrorMessage(HelloMessage.TYPE,
                                         'invalid/incomplete cert chain')
                self.transport.write(error_msg)
                self.machine.reject()


class ChallengeState(BaseState):
    def __init__(self, transport, server_crypto, key_store, *args, **kwargs):
        super(ChallengeState, self).__init__(*args, **kwargs)
        self.transport = transport
        self.server_crypto, self.key_store = server_crypto, key_store

    def _on_enter(self):
        self.transport.write(ChallengeMessage(self.server_crypto.cert_chain,
                                              self.key_store.my_nonce))
        self.machine.transition(ChallengeResponseWaitState)


class ChallengeResponseWaitState(BaseState):
    def __init__(self, transport, key_store, *args, **kwargs):
        super(ChallengeResponseWaitState, self).__init__(*args, **kwargs)
        self.transport, self.key_store = transport, key_store

    def _on_message(self, msg):
        if isinstance(msg, ChallengeResponseMessage):
            # NOTE: Here we don't care if this utility function short circuits
            # when the strings are of a different length, because the nonce
            # size isn't private. We only want to prevent timing attacks
            if not secure_string_equals(msg.server_nonce,
                                        self.key_store.my_nonce):
                error_msg = \
                    ErrorMessage(ChallengeResponseMessage.TYPE,
                                 'authentication error: nonce mismatch')
                self.transport.write(error_msg)
                self.machine.reject()
            else:
                try:
                    self.key_store.their_nonce = msg.client_nonce
                    self.key_store.their_key = msg.key
                    self.machine.transition(ResponseState)
                except (InvalidNonceException, InvalidKeyException) as e:
                    prop = 'key' if isinstance(e, InvalidKeyException) \
                           else 'nonce'
                    error_msg = ErrorMessage(ChallengeResponseMessage.TYPE,
                                             "invalid {}".format(prop))
                    self.transport.send(error_msg)
                    self.machine.reject()


class ResponseState(BaseState):
    def __init__(self, transport, key_store, *args, **kwargs):
        super(ResponseState, self).__init__(*args, **kwargs)
        self.transport, self.key_store = transport, key_store

    def _on_enter(self):
        self.transport.write(ResponseMessage(self.key_store.their_nonce,
                                             self.key_store.my_key))
        self.machine.accept()
