from sbp.psst.common.machine import BaseMachine
from sbp.psst.client.states.handshake import HandshakeState


class Machine(BaseMachine):
    def __init__(self, *args, **kwargs):
        super(Machine, self).__init__(HandshakeState, *args, **kwargs)
