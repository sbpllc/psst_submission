from sbp.psst.common.factory import create
from sbp.psst.client.machine import Machine

def create_client(*args, **kwargs):
    return create(Machine, *args, **kwargs)
