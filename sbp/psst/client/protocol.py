from playground.network.common.MessageHandler import \
        SimpleMessageHandlingProtocol
from playground.network.common.Protocol import StackingProtocolMixin
from playground.network.client.ClientApplicationServer import \
        ClientApplicationClient
from sbp.psst.common.messages.base import from_playground_msg, message_types
from sbp.psst.common.config import Config
from sbp.psst.client.factory import create_client as create_base
from sbp.ptcl.client.protocol import create_client as make_reliable


class PsstClientProtocol(SimpleMessageHandlingProtocol,
                         StackingProtocolMixin):
    """The client protocol implementation for PSST."""
    def __init__(self, *args, **kwargs):
        SimpleMessageHandlingProtocol.__init__(self, *args, **kwargs)

        for message_type in message_types:
            self.registerMessageHandler(message_type,
                                        self._handle_psst_message)

        self._config = Config.load()

    def connectionMade(self):
        self._client = create_base(self.transport, self.getHigherProtocol(),
                                   self._config.cert_chain,
                                   self._config.private_key)
        self._client.start()

    def _handle_psst_message(self, protocol, msg):
        psst_msg = from_playground_msg(msg)

        if psst_msg:
            self._client._on_message(psst_msg)


class PsstClient(ClientApplicationClient):
    """The client implemention for PSST."""
    Protocol = PsstClientProtocol


def create_client(higher_factory):
    """Creates and returns a new PSST client."""
    factory = PsstClient()
    factory.setHigherFactory(higher_factory)
    return make_reliable(factory)
