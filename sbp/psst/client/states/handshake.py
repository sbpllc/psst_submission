from sbp.psst.common.state import NestedStateMachine, BaseState
from sbp.psst.common.states.data import DataState
from sbp.psst.common.handshake_crypto import HandshakeCrypto, PublicKeyCrypto
from sbp.psst.common.crypto import InvalidKeyException, InvalidNonceException
from sbp.psst.common.encrypted_transport import EncryptedTransport
from sbp.psst.common.messages.handshake import HelloMessage, \
        ChallengeMessage, ChallengeResponseMessage, ResponseMessage, \
        ErrorMessage
from sbp.psst.common.util import secure_string_equals


class HandshakeState(NestedStateMachine):
    def __init__(self, transport, key_store, private_key, *args, **kwargs):
        public_key = PublicKeyCrypto()
        self.crypto = HandshakeCrypto(private_key, public_key)
        encrypted_transport = EncryptedTransport(transport, self.crypto)

        super(HandshakeState, self).__init__([
            HelloState(encrypted_transport, private_key),
            ChallengeWaitState(encrypted_transport, public_key, key_store),
            ChallengeResponseState(encrypted_transport, key_store),
            ResponseWaitState(key_store),
        ], *args, **kwargs)

    def _on_message(self, msg):
        decrypted_msg = self.crypto.decrypt(msg)
        super(HandshakeState, self)._on_message(decrypted_msg)

    def accept(self):
        super(HandshakeState, self).accept()
        self.machine.transition(DataState)

    def reject(self):
        super(HandshakeState, self).reject()
        self.machine.reject()


class HelloState(BaseState):
    def __init__(self, transport, client_crypto, *args, **kwargs):
        super(HelloState, self).__init__(*args, **kwargs)
        self.transport, self.client_crypto = transport, client_crypto

    def _on_enter(self):
        self.transport.write(HelloMessage(self.client_crypto.cert_chain))
        self.machine.transition(ChallengeWaitState)


class ChallengeWaitState(BaseState):
    def __init__(self, transport, server_crypto, key_store, *args, **kwargs):
        super(ChallengeWaitState, self).__init__(*args, **kwargs)
        self.transport, self.server_crypto = transport, server_crypto
        self.key_store = key_store

    def _on_message(self, msg):
        if isinstance(msg, ChallengeMessage):
            # TODO validate cert chain
            if True:
                self.server_crypto.set_public_key(msg.cert_chain)

                try:
                    self.key_store.their_nonce = msg.server_nonce

                    self.machine.transition(ChallengeResponseState)
                except InvalidNonceException:
                    error_msg = ErrorMessage(ChallengeMessage.TYPE,
                                             'invalid nonce')
                    self.transport.write(error_msg)
                    self.machine.reject()
            else:
                error_msg = ErrorMessage(ChallengeMessage.TYPE,
                                         'invalid/incomplete cert chain')
                self.transport.write(error_msg)
                self.machine.reject()


class ChallengeResponseState(BaseState):
    def __init__(self, transport, key_store, *args, **kwargs):
        super(ChallengeResponseState, self).__init__(*args, **kwargs)
        self.transport, self.key_store = transport, key_store

    def _on_enter(self):
        msg = ChallengeResponseMessage(server_nonce=self.key_store.their_nonce,
                                       client_nonce=self.key_store.my_nonce,
                                       key=self.key_store.my_key)
        self.transport.write(msg)
        self.machine.transition(ResponseWaitState)


class ResponseWaitState(BaseState):
    def __init__(self, key_store, *args, **kwargs):
        super(ResponseWaitState, self).__init__(*args, **kwargs)
        self.key_store = key_store

    def _on_message(self, msg):
        if isinstance(msg, ResponseMessage):
            if not secure_string_equals(msg.client_nonce,
                                        self.key_store.my_nonce):
                error_msg = ErrorMessage(ResponseMessage.TYPE,
                                         'error: nonce mismatch')
                self.transport.write(error_msg)
                self.machine.reject()
            else:
                try:
                    self.key_store.their_key = msg.key
                    self.machine.accept()
                except InvalidKeyException:
                    error_msg = ErrorMessage(ResponseMessage.TYPE,
                                             'invalid key')
                    self.transport.write(error_msg)
                    self.machine.reject()
