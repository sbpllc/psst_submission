def secure_string_equals(a, b):
    """A string compare that always operates in Theta(len(a)) time, and thus
    is not susceptible to a timing attack. It is "constant" when the strings
    are of unequal length, but for our purposes, the strings we compare are
    of (publicly) known length, so this isn't important.
    """
    if len(a) != len(b):
        return False

    def xor_bytes(result, (i, j)):
        return result | (ord(i) ^ ord(j))

    return reduce(xor_bytes, zip(a, b), 0) == 0
