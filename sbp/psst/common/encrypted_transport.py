class EncryptedTransport(object):
    def __init__(self, wrapped_transport, crypto):
        self._wrapped_transport = wrapped_transport
        self._crypto = crypto

    def write(self, msg):
        encrypted_msg = self._crypto.encrypt(msg)
        self._wrapped_transport.write(encrypted_msg)

    def close(self):
        self._wrapped_transport.close()

    def getPeer(self):
        return self._wrapped_transport.getPeer()

    def getHost(self):
        return self._wrapped_transport.getHost()
