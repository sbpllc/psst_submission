from playground.network.client.ClientApplicationTransport import \
        ClientApplicationTransport


class ProtocolBridge(object):
    def __init__(self, wrapped_protocol, transport):
        self._wrapped_protocol, self._transport = wrapped_protocol, transport
        self._proxy = None

    def connect(self, disconnect_callback):
        if not self._proxy:
            self._proxy = TransportProxy(self._transport, disconnect_callback)
            self._wrapped_protocol.makeConnection(self._proxy)

    def disconnect(self):
        if self._wrapped_protocol.connected:
            self._wrapped_protocol.connectionLost()


class TransportProxy(ClientApplicationTransport):
    def __init__(self, transport, disconnect_callback):
        # We are overriding a lot of behavior, so we don't want to call the
        # parent's constructor
        self._transport = transport
        self._disconnect_callback = disconnect_callback

    def write(self, data):
        self._transport.write(data)

    def loseConnection(self):
        self._disconnect_callback()

    def getPeer(self):
        return self._transport.getPeer()

    def getHost(self):
        return self._transport.getHost()
