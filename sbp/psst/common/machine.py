from sbp.psst.common.state import StateMachine
from sbp.psst.common.states.data import DataState
from sbp.psst.common.states.teardown import TeardownState, TeardownReplyState


class BaseMachine(StateMachine):
    def __init__(self, HandshakeState, transport, bridge, proxy, key_store,
                 crypto, private_key):
        super(BaseMachine, self).__init__([
            HandshakeState(transport, key_store, private_key),
            DataState(transport, bridge, proxy, crypto),
            TeardownState(transport, crypto),
            TeardownReplyState(transport, crypto),
        ])

        self.transport = transport

    def _on_message(self, msg):
        if self._current_state:
            self._current_state._on_message(msg)

    def accept(self):
        super(BaseMachine, self).accept()
        self.transport.close()

    def reject(self):
        super(BaseMachine, self).reject()
        self.transport.close()
