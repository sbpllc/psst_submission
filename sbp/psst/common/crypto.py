from os import urandom
from copy import copy
from Crypto.Cipher import AES
from Crypto.Util import Counter
from Crypto.Hash import HMAC, SHA256
from sbp.psst.common.messages.data import DataMessage
from sbp.psst.common.util import secure_string_equals


BITS_PER_BYTE = 8

KEY_BITS, NONCE_BITS = 256, 128
KEY_BYTES, NONCE_BYTES = KEY_BITS / BITS_PER_BYTE, NONCE_BITS / BITS_PER_BYTE


class KeyStore(object):
    def __init__(self):
        self.my_key, self.my_nonce = urandom(KEY_BYTES), urandom(NONCE_BYTES)
        self._their_key, self._their_nonce = None, None

    @property
    def their_key(self):
        return self._their_key

    @their_key.setter
    def their_key(self, their_key):
        if len(their_key) != KEY_BYTES:
            raise InvalidKeyException()
        self._their_key = their_key

    @property
    def their_nonce(self):
        return self._their_nonce

    @their_nonce.setter
    def their_nonce(self, their_nonce):
        if len(their_nonce) != NONCE_BYTES:
            raise InvalidNonceException()
        self._their_nonce = their_nonce


class InvalidNonceException(ValueError):
    pass


class InvalidKeyException(ValueError):
    pass


class Crypto(object):
    def __init__(self, key_store):
        self.key_store = key_store
        self._my_cipher, self._their_cipher = None, None

    @property
    def my_cipher(self):
        if self._my_cipher is None:
            self._my_cipher = self._create_cipher(self.key_store.my_key,
                                                  self.key_store.my_nonce)
        return self._my_cipher

    @property
    def their_cipher(self):
        if self._their_cipher is None:
            self._their_cipher = self._create_cipher(self.key_store.their_key,
                                                  self.key_store.their_nonce)
        return self._their_cipher

    def _create_cipher(self, key, iv_bytes):
        iv = int(iv_bytes.encode('hex'), 16)
        counter = Counter.new(NONCE_BITS, initial_value=iv)
        return AES.new(key, counter=counter, mode=AES.MODE_CTR)

    def encrypt(self, data='', Message=DataMessage):
        """Returns a message object ready to be sent of type Message.

        data -- The message's plaintext data (string/bytes)
        Message -- The class of message to be returned
        """

        cipher_text = self.my_cipher.encrypt(data)
        data, key = Message.TYPE + cipher_text, self.key_store.my_key
        return Message(data=cipher_text, mac=self._mac_data(data, key))

    def decrypt(self, encrypted_msg):
        """Decrypts and verifies an incoming message.
        Returns None if the HMAC does not verify, otherwise returns the
        message with the data unencrypted.

        encrypted_msg -- The message to be verified and decrypted
        """

        if self._verify_message(encrypted_msg):
            decrypted_msg = copy(encrypted_msg)
            decrypted_msg.data = self.their_cipher.decrypt(encrypted_msg.data)

            return decrypted_msg

    def _verify_message(self, msg):
        """Verifies message integrity to using HMAC.
        Returns True if the message verfies, False otherwise.

        msg -- the message to be verified
        """

        hmac = self._mac_data(msg.TYPE + msg.data, self.key_store.their_key)
        return secure_string_equals(hmac, msg.mac)

    def _mac_data(self, data, key):
        """Returns an HMAC of the given data.

        data -- The data to be HMAC'd
        key -- The key the data was encrypted with
        """

        # NOTE: We need a new one each time (update appends data to HMAC)
        hmac = HMAC.new(key, digestmod=SHA256)
        hmac.update(data)
        return hmac.hexdigest()
