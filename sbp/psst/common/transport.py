class Transport(object):
    def __init__(self, lower_transport):
        self._transport = lower_transport

    def write(self, msg):
        self._transport.write(msg.to_message().serialize())

    def close(self):
        self._transport.loseConnection()
