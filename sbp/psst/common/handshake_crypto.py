from playground.crypto import X509Certificate
from Crypto.Cipher.PKCS1_OAEP import PKCS1OAEP_Cipher
from Crypto.PublicKey import RSA
from copy import copy


class HandshakeCrypto(object):
    SENSITIVE_FIELDS = ('server_nonce', 'client_nonce', 'key')

    def __init__(self, private_key, public_key):
        self.private_key, self.public_key = private_key, public_key

    def _sensitive_fields(self, msg):
        for field in self.__class__.SENSITIVE_FIELDS:
            if hasattr(msg, field):
                yield field, getattr(msg, field)

    def encrypt(self, msg):
        """Encrypts the given message under the public key.

        msg -- the message to be encrypted
        """
        try:
            encrypted_msg = copy(msg)

            for field, value in self._sensitive_fields(msg):
                encrypted_value = self.public_key.encrypt(value)
                setattr(encrypted_msg, field, encrypted_value)

            return encrypted_msg
        except CannotEncryptException:
            return None
 

    def decrypt(self, msg):
        """Decrypts the given message using the private key.

        message -- the message to be decrypted
        """
        decrypted_msg = copy(msg)

        for field, value in self._sensitive_fields(msg):
            decrypted_value = self.private_key.decrypt(value)
            setattr(decrypted_msg, field, decrypted_value)

        return decrypted_msg


class PublicKeyCrypto(object):
    def __init__(self):
        self._encryptor = None

    def set_public_key(self, cert_chain):
        cert = X509Certificate.loadPEM(cert_chain[0])
        public_key_bytes = cert.getPublicKeyBlob()
        public_key = RSA.importKey(public_key_bytes)
        self._encryptor = PKCS1OAEP_Cipher(public_key, None, None, None)

    def encrypt(self, data):
        """Encrypts the given data under the public key.

        data -- the data to be encrypted
        """
        if self._encryptor:
            return self._encryptor.encrypt(data)
        else:
            raise CannotEncryptException()


class CannotEncryptException(Exception):
    pass


class PrivateKeyCrypto(object):
    def __init__(self, cert_chain, private_key_bytes):
        self.cert_chain = cert_chain

        private_key = RSA.importKey(private_key_bytes)
        self._decryptor = PKCS1OAEP_Cipher(private_key, None, None, None)

    def decrypt(self, data):
        """Decrypts the given data using the private key.

        data -- the data to be decrypted
        """
        return self._decryptor.decrypt(data)
