from playground.network.message.ProtoBuilder import MessageDefinition
from playground.network.message.StandardMessageSpecifiers import STRING
from playground.network.message import MessageData


class PSSTDataMessage(MessageDefinition):
    PLAYGROUND_IDENTIFIER = 'playground.base.PSSTDataMessage'
    MESSAGE_VERSION = '1.0'

    BODY = [
        ('MessageType', STRING),
        ('MAC', STRING),
        ('Data', STRING),
    ]


class BaseDataMessage(object):
    @classmethod
    def from_playground_msg(cls, msg):
        raise NotImplementedError

    def to_message(self, mac, data='', type_=None):
        msg = MessageData.GetMessageBuilder(PSSTDataMessage)

        msg['MessageType'].setData(type_ if type_ else self.__class__.TYPE)
        msg['MAC'].setData(mac)
        msg['Data'].setData(data)

        return msg


class DataMessage(BaseDataMessage):
    TYPE = 'DATA'

    def __init__(self, mac, data):
        self.mac, self.data = mac, data

    @classmethod
    def from_playground_msg(cls, msg):
        if msg.MessageType == cls.TYPE:
            return cls(mac=msg.MAC, data=msg.Data)

    def to_message(self):
        return super(DataMessage, self).to_message(mac=self.mac,
                                                   data=self.data)


factories = {DataMessage.TYPE: DataMessage.from_playground_msg}
message_types = [PSSTDataMessage]
