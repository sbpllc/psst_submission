from sbp.psst.common.messages.data import BaseDataMessage


class TeardownMessage(BaseDataMessage):
    def __init__(self, mac, data=''):
        self.mac = mac

    @classmethod
    def from_playground_msg(cls, msg):
        if msg.MessageType == cls.TYPE:
            return cls(msg.MAC)

    def to_message(self):
        return super(TeardownMessage, self) \
            .to_message(type_=self.__class__.TYPE, mac=self.mac)


class FinMessage(TeardownMessage):
    TYPE = 'FIN'


class FinAckMessage(TeardownMessage):
    TYPE = 'FINACK'


factories = {
    FinMessage.TYPE: FinMessage.from_playground_msg,
    FinAckMessage.TYPE: FinAckMessage.from_playground_msg,
}

message_types = []
