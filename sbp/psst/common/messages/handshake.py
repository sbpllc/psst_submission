from playground.network.message.ProtoBuilder import MessageDefinition
from playground.network.message.StandardMessageSpecifiers import STRING, LIST
from playground.network.message import MessageData


class PSSTHandshakeMessage(MessageDefinition):
    PLAYGROUND_IDENTIFIER = 'playground.base.PSSTHandshakeMessage'
    MESSAGE_VERSION = '1.0'

    BODY = [
        ('MessageType', STRING),
        ('Cert_Chain', LIST(STRING)),
        ('Key', STRING),
        ('Nonce_S', STRING),
        ('Nonce_C', STRING),
        ('Data', STRING),
    ]


class HandshakeMessage(object):
    @classmethod
    def from_playground_msg(cls, msg):
        raise NotImplementedError

    def to_message(self, type_=None, cert_chain=[], key='', server_nonce='',
                   client_nonce='', data=''):
        msg = MessageData.GetMessageBuilder(PSSTHandshakeMessage)

        msg['MessageType'].setData(type_ if type_ else self.__class__.TYPE)
        msg['Cert_Chain'].setData(cert_chain)
        msg['Key'].setData(key)
        msg['Nonce_S'].setData(server_nonce)
        msg['Nonce_C'].setData(client_nonce)
        msg['Data'].setData(data)

        return msg


class HelloMessage(HandshakeMessage):
    TYPE = 'HELLO'

    def __init__(self, cert_chain):
        self.cert_chain = cert_chain

    @classmethod
    def from_playground_msg(cls, msg):
        return cls(msg.Cert_Chain)

    def to_message(self):
        return super(HelloMessage, self).to_message(cert_chain=self.cert_chain)


class ChallengeMessage(HandshakeMessage):
    TYPE = 'CHALLENGE'

    def __init__(self, cert_chain, server_nonce):
        self.cert_chain, self.server_nonce = cert_chain, server_nonce

    @classmethod
    def from_playground_msg(cls, msg):
        return cls(msg.Cert_Chain, msg.Nonce_S)

    def to_message(self):
        return super(ChallengeMessage, self) \
            .to_message(cert_chain=self.cert_chain,
                        server_nonce=self.server_nonce)


class ChallengeResponseMessage(HandshakeMessage):
    TYPE = 'CHALLENGE/RESPONSE'

    def __init__(self, server_nonce, client_nonce, key):
        self.server_nonce, self.client_nonce = server_nonce, client_nonce
        self.key = key

    @classmethod
    def from_playground_msg(cls, msg):
        return cls(msg.Nonce_S, msg.Nonce_C, msg.Key)

    def to_message(self):
        return super(ChallengeResponseMessage, self) \
            .to_message(server_nonce=self.server_nonce,
                        client_nonce=self.client_nonce, key=self.key)


class ResponseMessage(HandshakeMessage):
    TYPE = 'RESPONSE'

    def __init__(self, client_nonce, key):
        self.client_nonce, self.key = client_nonce, key

    @classmethod
    def from_playground_msg(cls, msg):
        return cls(msg.Nonce_C, msg.Key)

    def to_message(self):
        return super(ResponseMessage, self) \
            .to_message(client_nonce=self.client_nonce, key=self.key)


class ErrorMessage(HandshakeMessage):
    def __init__(self, failed_state, message):
        self.failed_state, self.message = failed_state, message

    @classmethod
    def from_playground_msg(cls, msg):
        return cls(msg.MessageType[:-6], msg.Data)

    def to_message(self):
        return super(ErrorMessage, self) \
            .to_message(type_=(self.failed_state + 'ERROR'),
                        data=self.message)


factories = {
    HelloMessage.TYPE: HelloMessage.from_playground_msg,
    ChallengeMessage.TYPE: ChallengeMessage.from_playground_msg,
    ChallengeResponseMessage.TYPE: \
        ChallengeResponseMessage.from_playground_msg,
    ResponseMessage.TYPE: ResponseMessage.from_playground_msg,
}

# Add ErrorMessage factory
factories.update({(t + 'ERROR'): ErrorMessage.from_playground_msg
                   for t in factories.keys()})

message_types = [PSSTHandshakeMessage]
