from sbp.psst.common.messages.handshake import \
        factories as handshake_factories, message_types as handshake_msg_types
from sbp.psst.common.messages.data import \
        factories as data_factories, message_types as data_msg_types
from sbp.psst.common.messages.teardown import \
    factories as teardown_factories, message_types as teardown_msg_types


def from_playground_msg(msg):
    data = msg.data()
    factory = _factories.get(data.MessageType, lambda _: None)
    return factory(data)


_factories = {}
_sub_factories = [handshake_factories, data_factories, teardown_factories]

for sub_factory in _sub_factories:
    _factories.update(sub_factory)


message_types = handshake_msg_types + data_msg_types + teardown_msg_types
