from sbp.psst.common.transport import Transport
from sbp.psst.common.bridge import ProtocolBridge
from sbp.psst.common.proxy import DeliveryProxy
from sbp.psst.common.crypto import KeyStore, Crypto
from sbp.psst.common.handshake_crypto import PrivateKeyCrypto
from sbp.psst.common.encrypted_transport import EncryptedTransport


def create(type_, lower_transport, higher_protocol, cert_chain,
           private_key_bytes):
    key_store = KeyStore()
    crypto = Crypto(key_store)
    private_key = PrivateKeyCrypto(cert_chain, private_key_bytes)

    transport = Transport(lower_transport)
    encrypted_transport = EncryptedTransport(transport, crypto)
    bridge = ProtocolBridge(higher_protocol, encrypted_transport)
    delivery_proxy = DeliveryProxy(higher_protocol)

    return type_(transport, bridge, delivery_proxy, key_store, crypto,
                 private_key)
