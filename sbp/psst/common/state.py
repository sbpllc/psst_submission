class StateMachine(object):
    """A generalized DFA for representing the many steps in a stateful PTCL
    protocol interaction.
    """
    def __init__(self, states):
        self._states = {state.__class__: state for state in states}
        self._initial_state = states[0].__class__
        self._current_state = None

        # Machine reference must be late bound
        for state in states:
            state.machine = self

    def start(self):
        """Transition the machine to the first state provided during
        initialization (the initial state).
        """
        if self._current_state is None:
            self.transition(self._initial_state)

    def transition(self, state_type):
        """Transition the machine into a new state.

        state_type -- The class of the state to which to transition
                      (only the state instance provided during initialization
                      should be an instance of this class)
        """
        if self.in_state(state_type):
            # Don't transition to a state we're already in
            return

        if self._current_state:
            self._current_state._on_leave()

        if state_type:
            self._current_state = self._states[state_type]
            self._current_state._on_enter()
        else:
            self._current_state = None

    def in_state(self, state_type):
        """Test if a machine is in a given state.

        state_type -- The class of the state to test
        """
        if state_type is None:
            return self._current_state is None
        else:
            return isinstance(self._current_state, state_type)

    def _on_message(self, msg):
        """Propagate a message through the machine.
        Delivers the message to the current state of the machine.

        msg -- The message to deliver
        """
        if self._current_state:
            self._current_state._on_message(msg)

    def accept(self):
        """Accept a certain stream of inputs. By default, this just transitions
        to no state, but this should be extended to perform other teardown
        processes.
        """
        self.transition(None)

    def reject(self):
        """Reject a certain stream of inputs. By default, this just transitions
        to no state, but this should be extended to perform other teardown
        processes.
        """
        self.transition(None)


class BaseState(object):
    """The base class of all states used by a StateMachine."""
    def __init__(self):
        self.machine = None

    def _on_enter(self):
        """Notifies the state that its parent machine has just transitioned
        from another state to it.
        """
        pass

    def _on_message(self, msg):
        """Deliver a message to the state. (Called by the parent machine to
        pass messages down from the protocol)
        """
        pass

    def _on_leave(self):
        """Notifies the state that its parent machine has just transitioned
        from it to another state.
        """
        pass


class NestedStateMachine(StateMachine, BaseState):
    """A special combination State/StateMachine, which can be the child state
    of another state machine, but when entered, it behaves like a mini state
    machine. This is particularly useful for complex steps like the server
    handshake and termination.
    """
    def __init__(self, states, *args, **kwargs):
        StateMachine.__init__(self, states)
        BaseState.__init__(self, *args, **kwargs)

    def _on_enter(self):
        self.start()

    def _on_message(self, msg):
        StateMachine._on_message(self, msg)

    def accept(self):
        StateMachine.accept(self)

    def reject(self):
        StateMachine.reject(self)
