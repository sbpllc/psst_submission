class DeliveryProxy(object):
    def __init__(self, wrapped_protocol):
        self._wrapped_protocol = wrapped_protocol

    def deliver(self, data):
        self._wrapped_protocol.dataReceived(data)
