from playground.config import GlobalPlaygroundConfigData


class Config(object):
    CONFIG_NAME = 'playground.network.client.ClientBase' \
        '.connection_types.SECURE_STREAM'
    CERT_CHAIN_PROP = 'cert_chain'
    PRIVATE_KEY_PROP = 'private_key'

    @classmethod
    def load(cls):
        config = GlobalPlaygroundConfigData.getConfig(cls.CONFIG_NAME)
        cert_chain = cls._get_cert_chain(config)
        private_key = cls._get_private_key(config)

        return cls(cert_chain, private_key)

    @classmethod
    def _get_cert_chain(cls, config):
        cert_chain_list = config.get(cls.CERT_CHAIN_PROP, None)

        if cert_chain_list is None:
            raise MissingCertChainException()

        cert_chain_paths = map(lambda s: s.strip(), cert_chain_list.split(','))
        return [cls._read_file(p) for p in cert_chain_paths]

    @classmethod
    def _get_private_key(cls, config):
        private_key_path = config.get(cls.PRIVATE_KEY_PROP, None)

        if private_key_path is None:
            raise MissingPrivateKeyException()

        return cls._read_file(private_key_path)

    @staticmethod
    def _read_file(path):
        with open(path, 'r') as f:
            return f.read()

    def __init__(self, cert_chain, private_key):
        self.cert_chain, self.private_key = cert_chain, private_key


class ConfigError(Exception):
    pass


class MissingCertChainException(ConfigError):
    def __init__(self):
        msg = "Specify a public cert_chain starting with your public key " \
              "as a comma-separated list of files in {}.{} in your " \
              "playground config"

        super(MissingCertChainException, self) \
            .__init__(msg.format(Config.CONFIG_NAME, Config.CERT_CHAIN_PROP))


class MissingPrivateKeyException(ConfigError):
    def __init__(self):
        msg = "Specify the filename to your private key in {}.{} " \
              "in your playground config"

        super(MissingPrivateKeyException, self) \
            .__init__(msg.format(Config.CONFIG_NAME, Config.PRIVATE_KEY_PROP))
