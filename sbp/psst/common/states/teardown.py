from sbp.psst.common.state import BaseState
from sbp.psst.common.messages.teardown import FinMessage, FinAckMessage


class TeardownState(BaseState):
    def __init__(self, transport, crypto, *args, **kwargs):
        super(TeardownState, self).__init__(*args, **kwargs)
        self.transport, self.crypto = transport, crypto

    def _on_enter(self):
        finack = self.crypto.encrypt(Message=FinMessage)
        self.transport.write(finack)

    def _on_message(self, msg):
        if isinstance(msg, FinAckMessage):
            self.machine.accept()


class TeardownReplyState(BaseState):
    def __init__(self, transport, crypto, *args, **kwargs):
        super(TeardownReplyState, self).__init__(*args, **kwargs)
        self.transport, self.crypto = transport, crypto

    def _on_enter(self):
        finack = self.crypto.encrypt(Message=FinAckMessage)
        self.transport.write(finack)
        self.machine.accept()
