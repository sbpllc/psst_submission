from sbp.psst.common.state import BaseState
from sbp.psst.common.messages.data import DataMessage
from sbp.psst.common.messages.teardown import FinMessage
from sbp.psst.common.states.teardown import TeardownState, TeardownReplyState


class DataState(BaseState):
    def __init__(self, transport, bridge, proxy, crypto, *args, **kwargs):
        super(DataState, self).__init__(*args, **kwargs)

        self.transport, self.bridge, self.proxy = transport, bridge, proxy
        self.crypto = crypto

    def _on_enter(self):
        self.bridge.connect(self._on_disconnect)

    def _on_message(self, msg):
        if isinstance(msg, DataMessage):
            decrypted_msg = self.crypto.decrypt(msg)

            if decrypted_msg is not None:
                self.proxy.deliver(decrypted_msg.data)
        elif isinstance(msg, FinMessage):
            self.machine.transition(TeardownReplyState)

    def _on_disconnect(self):
        self.machine.transition(TeardownState)

    def _on_leave(self):
        self.bridge.disconnect()
