import unittest
from mock import Mock, patch
from tests.util.surrogate import fake_patch
from twisted.python.failure import Failure
from twisted.internet.error import ConnectionDone, ConnectionLost


class ClientApplicationTransport(object):
    pass


PG_NS = 'playground.network.client.ClientApplicationTransport'

with fake_patch(PG_NS + '.ClientApplicationTransport',
                new=ClientApplicationTransport) as m:
    from sbp.psst.common.bridge import ProtocolBridge, TransportProxy


class TestProtocolBridge(unittest.TestCase):
    def setUp(self):
        self.wrapped_protocol, self.transport = Mock(), Mock()
        self.callback = Mock()
        self.bridge = ProtocolBridge(self.wrapped_protocol, self.transport)

        self.wrapped_protocol.connected = False
        self.wrapped_protocol.makeConnection = Mock()

        self_ = self
        def setConnected(value):
            def setter(self):
                self_.wrapped_protocol.connected = value

            return setter

        self.wrapped_protocol.makeConnection.side_effect = setConnected(True)
        self.wrapped_protocol.connectionLost.side_effect = setConnected(False)

    def assertConnected(self):
        self.assertEquals(1, self.wrapped_protocol.makeConnection.call_count)
        (proxy,), _ = self.wrapped_protocol.makeConnection.call_args
        self.assertEquals(self.callback, proxy._disconnect_callback)

    def test_connect(self):
        self.bridge.connect(self.callback)
        self.assertConnected()

    def test_multiple_connect(self):
        self.bridge.connect(self.callback)
        self.assertConnected()
        self.bridge.connect(self.callback)
        self.assertConnected()

    def assertNotDisconnected(self):
        self.assertEquals([],
                          self.wrapped_protocol.connectionLost.call_args_list,
                          'connectionLost should not be called')

    def assertDisconnected(self):
        self.wrapped_protocol.connectionLost.assert_called_once_with()

    def test_disconnect_before_connect(self):
        self.bridge.disconnect()
        self.assertNotDisconnected()

    def test_disconnect_after_connect(self):
        self.bridge.connect(self.callback)
        self.bridge.disconnect()
        self.assertDisconnected()

    def test_multiple_disconnect_after_connect(self):
        self.bridge.connect(self.callback)
        self.bridge.disconnect()
        self.assertDisconnected()
        self.bridge.disconnect()
        self.assertDisconnected()


class TestTransportProxy(unittest.TestCase):
    def setUp(self):
        self.transport, self.callback = Mock(), Mock()
        self.proxy = TransportProxy(self.transport, self.callback)

    def test_get_peer_host(self):
        peer, host = Mock(), Mock()
        self.transport.getPeer.return_value = peer
        self.transport.getHost.return_value = host

        self.assertEquals(peer, self.proxy.getPeer())
        self.assertEquals(host, self.proxy.getHost())

    def test_write(self):
        data = Mock()
        self.proxy.write(data)
        self.transport.write.assert_called_once_with(data)

    def test_lose_connection(self):
        self.proxy.loseConnection()
        self.callback.assert_called_once_with()


if __name__ == '__main__':
    unittest.main()
