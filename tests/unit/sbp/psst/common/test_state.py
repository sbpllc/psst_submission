import unittest
from mock import Mock
from sbp.psst.common.state import StateMachine, BaseState


class TestStateMachine(unittest.TestCase):
    def setUp(self):
        self.states = [StateOne(), StateTwo()]
        self.machine = StateMachine(self.states)

    def test_state_machine_reference(self):
        for state in self.states:
            self.assertEqual(self.machine, state.machine)

    def test_start_initial_state(self):
        self.states[0]._on_enter = Mock()
        self.assertTrue(self.machine.in_state(None))
        self.machine.start()
        self.states[0]._on_enter.assert_called_once_with()
        self.assertTrue(self.machine.in_state(StateOne))

    def test_transitions(self):
        self.states[0]._on_leave = Mock()
        self.states[1]._on_enter = Mock()

        self.machine.start()
        self.machine.transition(StateTwo)
        self.assertTrue(self.machine.in_state(StateTwo))

        self.states[0]._on_leave.assert_called_once_with()
        self.states[1]._on_enter.assert_called_once_with()

    def test_double_transition(self):
        self.states[0]._on_leave = Mock()
        self.states[1]._on_enter = Mock()

        self.machine.start()
        self.machine.transition(StateTwo)
        self.machine.transition(StateTwo)

        self.states[0]._on_leave.assert_called_once_with()
        self.states[1]._on_enter.assert_called_once_with()

    def test_accept_no_current_state(self):
        self.machine.accept()

    def test_accept_state_leaves_current_state(self):
        self.machine.start()
        self.states[0]._on_leave = Mock()

        self.machine.accept()

        self.states[0]._on_leave.assert_called_with()
        self.assertFalse(self.machine.in_state(StateOne))
        self.assertFalse(self.machine.in_state(StateTwo))

    def test_reject_no_current_state(self):
        self.machine.reject()

    def test_reject_state(self):
        self.machine.start()
        self.states[0]._on_leave = Mock()

        self.machine.reject()

        self.states[0]._on_leave.assert_called_with()
        self.assertFalse(self.machine.in_state(StateOne))
        self.assertFalse(self.machine.in_state(StateTwo))


class TestState(unittest.TestCase):
    def test_state(self):
        state = BaseState()
        _ = state.machine
        state._on_enter()
        msg = Mock()
        state._on_message(msg)
        state._on_leave()


# States are uniquely identified by their __class__, but there's no clean way
# to fake the class of a mock, so it's easier to just use real children
class StateOne(BaseState):
    pass


class StateTwo(BaseState):
    pass


if __name__ == '__main__':
    unittest.main()
