import unittest
from sbp.psst.common.util import secure_string_equals


class TestUtil(unittest.TestCase):
    def test_secure_string_equals(self):
        STR = 'abcdefg'
        self.assertTrue(secure_string_equals(STR, STR))
        self.assertFalse(secure_string_equals(STR, STR + '1'))


if __name__ == '__main__':
    unittest.main()
