import unittest
from mock import patch
from tests.util.surrogate import fake_patch

# Mock out the playground dependency so we can test in isolation
PLAYGROUND_NS = 'playground.network.message'
MSG_DEF_NS = PLAYGROUND_NS + '.ProtoBuilder.MessageDefinition'
MSG_SPEC_NS = PLAYGROUND_NS + '.StandardMessageSpecifiers'
MSG_DATA_NS = PLAYGROUND_NS + '.MessageData'


class MessageDefinition(object):
    pass

with fake_patch(MSG_DEF_NS, new=MessageDefinition) as _, \
        fake_patch(MSG_SPEC_NS + '.STRING') as STRING, \
        fake_patch(MSG_DATA_NS) as MessageData:
    from sbp.psst.common.crypto import KeyStore, Crypto
    from sbp.psst.common.messages.data import DataMessage


class TestCrypto(unittest.TestCase):
    def setUp(self):
        self.my_keystore = KeyStore()
        self.their_keystore = KeyStore()

        # Exchange keys and nonces
        self.my_keystore.their_key = self.their_keystore.my_key
        self.my_keystore.their_nonce = self.their_keystore.my_nonce

        self.their_keystore.their_key = self.my_keystore.my_key
        self.their_keystore.their_nonce = self.my_keystore.my_nonce

        self.my_crypto = Crypto(self.my_keystore)
        self.their_crypto = Crypto(self.their_keystore)

    def test_encrypt_decrypt_message(self):
        DATA = 'This is a mock message!'
        encrypted_msg = self.my_crypto.encrypt(Message=MessageMock, data=DATA)
        decrypted_msg = self.their_crypto.decrypt(encrypted_msg)

        self.assertIsNotNone(decrypted_msg)
        self.assertEqual(DATA, decrypted_msg.data)

    def test_encrypt_decrypt_string(self):
        DATA = 'This is a mock message!'
        encrypted_msg = self.my_crypto.encrypt(DATA)
        self.assertIsInstance(encrypted_msg, DataMessage)

        decrypted_msg = self.their_crypto.decrypt(encrypted_msg)
        self.assertIsNotNone(decrypted_msg)
        self.assertIsInstance(decrypted_msg, DataMessage)
        self.assertEqual(DATA, decrypted_msg.data)


class MessageMock(object):
    TYPE = 'mock'

    def __init__(self, data, mac):
        self.data, self.mac = data, mac


if __name__ == '__main__':
    unittest.main()
