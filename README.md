PSST Implementation
===================

*Author: SBP, LLC*


## Usage Instructions

To use SBP's PSST implementation, add the following to your playground configuration's `connection_types`.

```
=====connection_types=====
     list = RAW, RELIABLE_STREAM, SECURE_STREAM

======RAW======
      client = None
      server = None

======RELIABLE_STREAM======
      client = sbp.ptcl.client.protocol.create_client
      server = sbp.ptcl.server.protocol.create_server

======SECURE_STREAM======
      client = sbp.psst.client.protocol.create_client
      server = sbp.psst.server.protocol.create_server
      private_key = keys/private.key
      cert_chain = certs/my.cert, certs/root.cert
```

Note that `private_key` is the path to your private key and cert_chain is a comma-separated list of paths to your certs (starting with your cert and ending optionally with the root).

Also note that PSST must be used in conjuncation with PTCL.
